﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ex2API.Models;
using Ex2API.Services;
using Microsoft.AspNetCore.Mvc;

namespace Ex2API.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase {
        private readonly DataService _dataService;

        public static readonly LogService _logService = new LogService(null);

        public DataController (DataService dataservice) {

            _dataService = dataservice;
        }

        [HttpGet]
        public ActionResult<List<DataObject>> Get() {
            List<DataObject> data = _dataService.Get();
            DataLog log = new DataLog();
            log.responce = data;
            log.request = "GET";
            log.date = DateTime.UtcNow;


            _logService.Create(log);
            return data;
        }

        [HttpGet ("id/{id:length(24)}", Name = "GetById")]
        public ActionResult<DataObject> GetID (string id) {
            DataObject data = _dataService.GetID(id);

            if (data == null) {
                return NotFound();
            }

            List<DataObject> list = new List<DataObject>();
            DataLog log = new DataLog ();
            log.request = "GET BY ID : " + id;
            log.date = DateTime.UtcNow;
            list.Add (data);
            log.responce = list;

            _logService.Create(log);
            return data;
        }
        //* Format date YYYYMMDD
        [HttpGet ("date/{date:length(8)}", Name = "GetByDate")]
        public ActionResult<DataObject> GetDate(int date) {
            DataObject data = _dataService.GetDate(date);

            if (data == null) {
                return NotFound();
            }
            List<DataObject> list = new List<DataObject>();
            DataLog log = new DataLog();
            
            log.request = "GET BY DATE : " + date;
            log.date = DateTime.UtcNow;
            
            list.Add(data);
            log.responce = list;
_logService.Create(log);
            return data;
        }

    }
}