using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ex2API.Models;
using Ex2API.Services;
using Microsoft.AspNetCore.Mvc;

namespace Ex2API.Controllers {

    [Route ("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase {
        private readonly LogService _logService;

        public LogController (LogService logService) {
            _logService = logService;
        }

        [HttpGet]
        public ActionResult<List<DataLog>> Get () {

            return _logService.Get();

        }
         [HttpGet ("id/{id:length(24)}", Name = "LogById")]
        public ActionResult<DataLog> GetID (string id) {
            DataLog log = _logService.GetID(id);

            if (log == null) {
                return NotFound ();
            }

            return log;
        }
        //* Format date YYYYMMDD
        [HttpGet ("date/{date:length(8)}", Name = "LogByDate")]

        public ActionResult<List<DataLog>> GetDate (int date) {
          IEnumerable<DataLog> log = _logService.GetDate(date);


            if (log == null) {
                return NotFound ();
            }
            
            return Ok(log);
        }

    }
}