using System;
using System.Timers;
using Ex2API.Models;
using Ex2API.Services;

namespace Ex2API.MainCode {
    public static class Core {
        public static readonly DataService _dataService = new DataService (null);
        public static DataObject lastEntry = new DataObject ();
        private static bool CheckStoredCriteria (DataObject data) {

            // Check if we are the weekend
            if (data.montrealDate.DayOfWeek != DayOfWeek.Sunday && data.montrealDate.DayOfWeek != DayOfWeek.Sunday) {
                // check if the stock market just close and if the last entry was yesterday 
                if ((data.montrealDate.Hour >= 16 && (lastEntry.montrealDate.DayOfYear != data.montrealDate.DayOfYear || lastEntry.montrealDate.Year != data.montrealDate.Year))) {
                    return true;
                } 
                // check if the stock market is not yet open or if the volume is less than 30000
                else if (((data.montrealDate.Hour <= 8 || (data.montrealDate.Hour == 9 && data.montrealDate.Minute <= 29))) || data.stockData.volume < 30000) {
                    return false;
                } 
                // check if the stock market is open and if the volume is is greater or equal than 30000
                else if ((data.montrealDate.Hour <= 16 && data.stockData.volume >= 10000) && (lastEntry.montrealDate.DayOfYear != data.montrealDate.DayOfYear || lastEntry.montrealDate.Year != data.montrealDate.Year)) {
                    return true;
                }
                return false;

            }
            return false;
        }
        public static async void OnTimedEvent (Object source, ElapsedEventArgs e) {

            DataObject data = new DataObject ();
            data.stockData = await externalApiRequest.GetStock ();
            data.montrealWeather = await externalApiRequest.GetWeather ();
            data.montrealDate = await externalApiRequest.GetTime (Position.Montreal);
            data.moscowDate = await externalApiRequest.GetTime (Position.Moscow);
            data.dateMeaning = await externalApiRequest.GetMeaning (data.montrealDate.Day, data.montrealDate.Month);
            data.lastestNews = await externalApiRequest.GetNews ();

            if (CheckStoredCriteria(data)) {
                lastEntry = data;
                _dataService.Create (data);
                
            }
        }

    }
}