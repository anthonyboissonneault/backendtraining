namespace Ex2API.Models
{
   public class CgiNews {
        public string title { get; set; }
        public string summary { get; set; }
        public string url { get; set; }
        public string date { get; set; }
    }
}