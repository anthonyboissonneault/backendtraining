using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace Ex2API.Models {
  
    public class DataObject {
        [BsonId]
        [BsonRepresentation (BsonType.ObjectId)]
        public string Id { get; set; }
        public DateTime moscowDate { get; set; }
        public DateTime montrealDate { get; set; }
        public Weather montrealWeather { get; set; }
        public CgiNews[] lastestNews { get; set; }
        public Stock stockData { get; set; }
        public string dateMeaning { get; set; }
    }

}