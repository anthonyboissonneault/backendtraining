namespace Ex2API.Models
{
    public class Stock
    {
        
        public decimal sharePrice { get; set; }
        public decimal volume { get; set; }
        public string sharePriceChange { get; set; }

    
    }
}