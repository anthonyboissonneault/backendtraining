using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
namespace Ex2API.Models
{
    public class DataLog
    {
        [BsonId]
        [BsonRepresentation (BsonType.ObjectId)]
        public string Id { get; set; }
        public string request {get; set;}
        public DateTime date {get; set;}
        public List<DataObject> responce {get;set;}
    }
}