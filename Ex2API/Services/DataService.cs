using System;
using System.Collections.Generic;
using System.Linq;
using Ex2API.Models;
using MongoDB.Driver;

namespace Ex2API.Services {
    public class DataService {
        private readonly IMongoCollection<DataObject> _datas;

        public DataService (IDatabaseSettings settings) {

            MongoClient client = new MongoClient ("mongodb://192.168.18.25:27017");
            IMongoDatabase database = client.GetDatabase ("admin");

            _datas = database.GetCollection<DataObject> ("stockMarketDb");
        }

        public List<DataObject> Get () =>
            _datas.Find (data => true).ToList ();

        // * format date  YYYYMMDD  
        public DataObject GetDate (int date) {
            int[] splitDate = new int[3];

            //Year
            splitDate[0] = int.Parse (date.ToString ().Substring (0, 4));

            //Month
            splitDate[1] = int.Parse (date.ToString ().Substring (4, 2));

            //Day
            splitDate[2] = int.Parse (date.ToString ().Substring (6, 2));


            DateTime tempDate = new DateTime(splitDate[0],splitDate[1],splitDate[2]);


            IEnumerable<DataObject> data = from _stock in _datas.Find(t => true).ToEnumerable()
                where _stock.montrealDate.Year ==  tempDate.Year &&
                    _stock.montrealDate.Month ==  tempDate.Month &&
                    _stock.montrealDate.Day ==  tempDate.Day
                select _stock;
                
            return data.FirstOrDefault ();
        }
        public DataObject GetID (string id) =>
            _datas.Find<DataObject> (data => data.Id == id).FirstOrDefault ();

        public DataObject Create (DataObject data) {
            _datas.InsertOne (data);
            return data;
        }

        public void Update (string id, DataObject dataIn) =>
            _datas.ReplaceOne (data => data.Id == id, dataIn);

        public void Remove (DataObject dataIn) =>
            _datas.DeleteOne (data => data.Id == dataIn.Id);

        public void Remove (string id) =>
            _datas.DeleteOne (data => data.Id == id);
    }
}