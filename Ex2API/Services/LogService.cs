using System;
using System.Collections.Generic;
using System.Linq;
using Ex2API.Models;
using MongoDB.Driver;

namespace Ex2API.Services {
    public class LogService {

        private readonly IMongoCollection<DataLog> _datas;

        public LogService (IDatabaseSettings settings) {

            MongoClient client = new MongoClient ("mongodb://192.168.18.25:27017");
            IMongoDatabase database = client.GetDatabase ("admin");

            _datas = database.GetCollection<DataLog>("Log");
        }

        public List<DataLog> Get () =>
            _datas.Find(data => true).ToList ();
        public DataLog GetID (string id) =>
            _datas.Find<DataLog>(data => data.Id == id).FirstOrDefault();

        // * format date  YYYYMMDD  
        public IEnumerable<DataLog> GetDate (int date) {
            int[] splitDate = new int[3];

            //Year
            splitDate[0] = int.Parse (date.ToString ().Substring (0, 4));

            //Month
            splitDate[1] = int.Parse (date.ToString ().Substring (4, 2));

            //Day
            splitDate[2] = int.Parse (date.ToString ().Substring (6, 2));

            DateTime tempDate = new DateTime (splitDate[0], splitDate[1], splitDate[2]);

            IEnumerable<DataLog> data = from _stock in _datas.Find(t => true).ToEnumerable()
            where _stock.date.Year ==  tempDate.Year &&
                _stock.date.Month ==  tempDate.Month &&
                _stock.date.Day ==  tempDate.Day
            select _stock;

            return data;
        }
        public DataLog Create (DataLog data) {
            _datas.InsertOne(data);
            return data;
        }

    }
}