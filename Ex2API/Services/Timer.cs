using Ex2API.MainCode;
namespace Ex2API.Services {
    public static class Timer {

        private static System.Timers.Timer aTimer;
        

        public static void SetTimer () {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer (300000);

            // Hook up the Elapsed event for the timer.
            aTimer.Elapsed += Core.OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

    }

}