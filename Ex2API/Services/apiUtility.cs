using System.Threading.Tasks;

using System.Net.Http;


namespace Ex2API.Services
{
    public static class apiUtility{
         public static async Task<string> callApi (string url) {
            string json;
            using (HttpClient client = new HttpClient ())

            using (HttpResponseMessage res = await client.GetAsync (url)) {
                json = await res.Content.ReadAsStringAsync ();
            }
            return json;
        }
    }
     
}