using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Ex2API.Models;
using Newtonsoft.Json.Linq;
namespace Ex2API.Services {
      public enum Position {
        Moscow,
        Montreal
    }
    public static class externalApiRequest {

        private const string WeatherKey = "6e9e005f5e5991338a2e05832f96b29a";
        private const string StockKey = "J1QMMRV516MH93F3";
        public static async Task<Weather> GetWeather () {

            string baseUrl = "https://api.openweathermap.org/data/2.5/weather?id=6077243&units=metric&appid=" + WeatherKey;
            string json = await apiUtility.callApi(baseUrl);

            JObject _weather = JObject.Parse (json);

            Weather weather = new Weather ();
            weather.sky = (string)_weather["weather"][0]["main"];

            weather.metricTemp =(decimal)_weather["main"]["temp"];
            return weather;

        }

        public static async Task<DateTime> GetTime (Position position) {

            string baseUrl = "http://api.geonames.org/timezoneJSON?" + (position == Position.Montreal ? "lat=45.5100&lng=-73.5958" : "lat=55.7558&lng=37.6173") + "&username=cddl1445";
            string json = await apiUtility.callApi(baseUrl);

            JObject _time = JObject.Parse(json);

            DateTime time = (DateTime) _time["time"];

            return time;

        }

        public static async Task<Stock> GetStock () {

            string baseUrl = "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GIB-A.To&apikey=" + StockKey;
            string json = await apiUtility.callApi(baseUrl);

            JObject _stock = JObject.Parse (json);

            Stock stock = new Stock ();
           stock.sharePrice = _stock["Global Quote"].Value<decimal>("05. price");
           stock.sharePriceChange =  _stock["Global Quote"].Value<string>("10. change percent");
          stock.volume =_stock["Global Quote"].Value<decimal>("06. volume");
            return stock;

        }
        public static async Task<string> GetMeaning (int day, int month) {

            string baseUrl = "http://numbersapi.com/" + month + "/" + day + "/date?json";
            string json = await apiUtility.callApi(baseUrl);
            JObject _temp = JObject.Parse (json);

            return (string) _temp["text"];

        }
  

//* DATA GIVEN BY THE NEWS API
 /* 
                    
           {
             "version": "https://jsonfeed.org/version/1",
            "title": null,
            "description": "Latest Financial News for GIB-A.To",
            "items": [
                {
                    "guid": "7a4b78fb-0059-3c04-b585-bbfeeadd62ca",
                    "url": "https://finance.yahoo.com/news/interested-cgi-inc-tse-gib-165207000.html?.tsrc=rss",
                    "title": "Interested In CGI Inc. (TSE:GIB.A)? Here&#39;s What Its Recent Performance Looks Like",
                    "content_html": "Assessing CGI Inc.&#39;s (TSX:GIB.A) past track record of performance is an insightful...",
                    "summary": "Assessing CGI Inc.&#39;s (TSX:GIB.A) past track record of performance is an insightful...",
                    "date_published": "2019-09-25T16:52:07.000Z"
                },
                {
                    "guid": "cfdc0838-2aa4-3996-965c-ae0e32decb9c",
                    "url": "https://finance.yahoo.com/news/national-bank-canada-cgi-team-134100413.html?.tsrc=rss",
                    "title": "National Bank of Canada and CGI team up to enhance the bank's payments ecosystem",
                    "content_html": "MONTRÉAL, Sept. 23, 2019 /PRNewswire/ - CGI (GIB-A.TO) (GIB) has been awarded a contract ...",
                    "summary": "MONTRÉAL, Sept. 23, 2019 /PRNewswire/ - CGI (GIB-A.TO) (GIB) has been awarded a contract...",
                    "date_published": "2019-09-23T13:41:00.000Z"
                }, ...
            }
    
*/
          public static async Task<CgiNews[]> GetNews () {

            string baseUrl = "https://feed2json.org/convert?url=https%3A%2F%2Ffeeds.finance.yahoo.com%2Frss%2F2.0%2Fheadline%3Fs%3DGIB-A.To%26region%3DUS%26lang%3Den-US";
            string json = await apiUtility.callApi(baseUrl);
            CgiNews[] news = new CgiNews[5];

            JObject _temp = JObject.Parse (json);

            for (int i = 0; i < 5; i++) {
                CgiNews _news = new CgiNews ();
                _news.url = (string) _temp["items"].Children ().ToList () [i].Value<string> ("url");
                _news.title = (string) _temp["items"].Children ().ToList () [i].Value<string> ("title");
                _news.summary = (string) _temp["items"].Children ().ToList () [i].Value<string> ("summary");
                _news.date = (string) _temp["items"].Children ().ToList () [i].Value<string> ("date_published");
                news[i] = _news;
            }
            return news;

        }

      

    }
}