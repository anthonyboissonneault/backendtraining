﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using System.Text;



namespace Ex1API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CityController : ControllerBase
    {
        // GET api/City
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> Get()
        {
            
           
            
            DataService dts = new DataService();
            detailedCity dc = await dts.GetValues();
            

            //Write the result in log.json
            FileStream stream = new FileStream("./log.json",FileMode.Append);
            
            using(StreamWriter writer = new StreamWriter(stream, Encoding.UTF8)){
                writer.WriteLine("{\"Date\":\""+DateTime.Now+"\", \"Result\": ["+ JsonConvert.SerializeObject(dc)+"]},");
            }

            return Ok(dc) ;
             
 
            
        }

        
        
      
    }
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        // GET api/Log
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            string res;

            //Read in log.json
            FileStream stream = new FileStream("./log.json",FileMode.Open);
            
            using(StreamReader read = new StreamReader(stream, Encoding.UTF8)){
               res = read.ReadToEnd();
            }
            return Ok(JsonConvert.DeserializeObject("["+res+"]"));
        }
      
      
    }
  
}
