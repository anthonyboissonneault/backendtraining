using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace Ex1API
{
       public class DataService
    {
        public const string valueKeyApi = "6f41a8fdbd05809a9204d7e96c9bfc24";
         
        public static countries[] allCountry;  
        
    public Random rnd = new Random(); 

    //Get values of all country in a online api than take one random (call GetRegion and GetSpeakerMasterCountry also)
        public async Task<detailedCity> GetValues()
        {
            try
            {
    
                countries chosenCountry; 
            
              string baseUrl = "https://restcountries.eu/rest/v2/all?fields=name;alpha2Code;population;languages";
              using (HttpClient client = new HttpClient())

              using (HttpResponseMessage res = await client.GetAsync(baseUrl))
            
                 {
                     if(allCountry == null){
                        string json = await res.Content.ReadAsStringAsync();
                

                        allCountry =  JsonConvert.DeserializeObject<countries[]>(json);
              
                        
                    
                    }
                   
                     citys city = new citys();
                    for (;;)
                    {
                        chosenCountry = allCountry[rnd.Next(0,allCountry.Count()-1)];
                        regions region = await GetRegion(chosenCountry);
                        if(region != null){
                            city = await GetCity(chosenCountry, region);
                            if(city != null){
                                break; 
                            }  
                        }
                       
                    }
                   
                   
                    countries contry = chosenCountry;
                    countries speaker = GetSpeakerMasterCountry(chosenCountry);
                    return GetResultFormated(city,contry,speaker);
                }
            }

            catch (System.Exception)
            {
                throw;
            }
        }

         //Get values of all country in a online api than take one random (also call GetCity)
         public async Task<regions> GetRegion(countries chosenCountry)
        {
            try
            {
                regions chosenRegion; 
     
                string baseUrl = "http://battuta.medunes.net/api/region/"+chosenCountry.alpha2Code+"/all/?key="+valueKeyApi;
                using (HttpClient client = new HttpClient())

                using (HttpResponseMessage res = await client.GetAsync(baseUrl))
            
                {
                    string json = await res.Content.ReadAsStringAsync();
                

                     
                regions[] region = new regions[]{};
                try{
                    region =  JsonConvert.DeserializeObject<regions[]>(json);
                }catch{
                    await GetValues();
                }
                    
                    if(region.Count() > 1){
                        chosenRegion = region[rnd.Next(0,region.Count()-1)];
                        return chosenRegion;
                    }
                    else{
                        return null;
                    }
                    
            }
    
}
catch (System.Exception)
{
    
    throw;
}
        }

//Get values of all country in a online api than take one random 
          public async Task<citys> GetCity(countries chosenCountry, regions chosenRegion)
        {
            try
            {
                citys chosenCity;  

                string baseUrl = "http://battuta.medunes.net/api/city/"+chosenCountry.alpha2Code+"/search/?region="+chosenRegion.region+"&key="+valueKeyApi;
            
                using (HttpClient client = new HttpClient())

                using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                {
                    string json = await res.Content.ReadAsStringAsync();
                citys[] cities = new citys[]{};
                try{
                    cities =  JsonConvert.DeserializeObject<citys[]>(json);
                }catch{
                    await GetValues();
                }
                    

                    
                    if(cities.Count() > 0){
                        chosenCity = cities[rnd.Next(0,cities.Count()-1)];
                        return chosenCity;
                    }
                    else{
                       return null;
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
        }

//bubble sort the country by population and find the most populated contry speaking the language of the selected country
        public countries GetSpeakerMasterCountry(countries chosenCountry){
            try
            {
                
                countries SpeakerMasterCountry; 
                countries temp = new countries();

                for (int write = 0; write < allCountry.Length; write++) {
                    for (int sort = 0; sort < allCountry.Length - 1; sort++) {
                        if (allCountry[sort].population < allCountry[sort + 1].population) {
                            temp = allCountry[sort + 1];
                            allCountry[sort + 1] = allCountry[sort];
                            allCountry[sort] = temp;
                        }
                    }
                }

                foreach (var C in allCountry  )
                {
                    if(C.languages[0].iso639_2 == chosenCountry.languages[0].iso639_2){
                        SpeakerMasterCountry = C;
                        return SpeakerMasterCountry;
                    }
                }

                return null;
            }
            catch (System.Exception)
            {
                
                throw;
            }
        } 
// Put all info in a custom object
        public detailedCity GetResultFormated(citys city, countries contry, countries speaker ){
            detailedCity dc = new detailedCity();
            dc.cityName = city.city;
            dc.countryName = contry.name;
            dc.language = contry.languages[0].name;
            dc.mostPopulateSameLanguage = speaker.name;

            return dc;
        } 

    }
}
